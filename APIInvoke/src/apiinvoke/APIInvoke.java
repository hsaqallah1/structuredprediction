/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apiinvoke;

import edu.stanford.nlp.simple.Document;
import edu.stanford.nlp.simple.Sentence;
import edu.stanford.nlp.trees.Tree;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;



/**
 *
 * @author hsaqallah
 */
public class APIInvoke {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        JSONParser parser = new JSONParser();
        FileOutputStream fout;
        ObjectOutputStream oos;

        try {     
            fout = new FileOutputStream("commandnlp.out");
            oos = new ObjectOutputStream(fout);

            Object obj = parser.parse(new FileReader(".\\val_commands.json"));

            JSONArray array =  (JSONArray) obj;
            for (int i = 0 ; i < array.size(); i++) {
                JSONObject el = (JSONObject) array.get(i);
                String originalSent = (String)el.get("nl_command_statment");
                long sentId = (Long)el.get("id");
                oos.writeObject(Utils.NLPizeSentence(originalSent));
         
            }
            oos.close();  
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }        
        
        
        
       //Testing restuls
        SentenceNLP theQueue;
           try {
    FileInputStream fin = new FileInputStream("commandnlp.out");
    ObjectInputStream ois = new ObjectInputStream(fin);
    while(true) {
        theQueue = (SentenceNLP) ois.readObject();
        System.out.println(theQueue.getCommand());
    }
        //ois.close();
    }
   catch (Exception e) { e.printStackTrace(); }
           
        }
        
}

   
