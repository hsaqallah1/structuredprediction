/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apiinvoke;

import edu.stanford.nlp.simple.Sentence;
import java.io.Serializable;

/**
 *
 * @author hsaqallah
 */
public class SerializableSentence extends Sentence implements Serializable {

    public SerializableSentence(String text) {
        super(text);
    }
    
}
