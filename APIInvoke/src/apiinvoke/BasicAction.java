/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apiinvoke;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author hsaqallah
 */
public class BasicAction extends AbstractAction{
    
    private long actionId;
    private String actionName;
    private JSONObject actionParams;

    public long getActionId() {
        return actionId;
    }

    public String getActionName() {
        return actionName;
    }

    public JSONObject getActionParams() {
        return actionParams;
    }

    public BasicAction(JSONObject jsonO, long level) {
        super(level);
        if((jsonO.get("id") instanceof String)){
            actionId = Long.parseLong((String)jsonO.get("id"));
        }
        else {
            actionId = (Long)jsonO.get("id");
        }
        actionName = (String)jsonO.get("name");
        actionParams = (JSONObject)jsonO.get("params");
    }
   
}
