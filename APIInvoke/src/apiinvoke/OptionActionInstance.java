/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apiinvoke;

import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author hsaqallah
 */
class OptionActionInstance extends AbstractAction {
    
    private ArrayList<BasicAction> options;

    public ArrayList<BasicAction> getOptions() {
        return options;
    }

    //This class supports single options as well. If o happens to be a single option, then the options array has a single element only. 
    public OptionActionInstance(JSONObject o, long level) {
        super(level);
        JSONArray JSONOptions = (JSONArray)o.get("options");
        options = new ArrayList<>();
        if (JSONOptions != null) {
            for (int i = 0; i < JSONOptions.size(); i++) {
                options.add(new BasicAction((JSONObject)JSONOptions.get(i),i));
            }
        } 
        else {
            options.add(new BasicAction((JSONObject)o,0l));
        }
        
    }
    
    public boolean isOneOption() {
        return options.size() == 1;
    }
}
