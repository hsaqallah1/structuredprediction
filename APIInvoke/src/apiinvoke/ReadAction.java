/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apiinvoke;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author husamsaq
 */
public class ReadAction {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JSONParser parser = new JSONParser();
        FileOutputStream fout;
        ObjectOutputStream oos;

        try {     

            Object obj = parser.parse(new FileReader(".\\actionkbv3.json"));
            
            fout = new FileOutputStream("action-nlp.out");
            oos = new ObjectOutputStream(fout);


            JSONArray array =  (JSONArray) obj;
            for (Object e:array) {
                JSONObject jsonO = (JSONObject) e;
                oos.writeObject(new API(jsonO));
//                
//                System.out.println();

            }
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }        
        // TODO code application logic here
    }
    
}
