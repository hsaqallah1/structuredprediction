/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apiinvoke;

import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author hsaqallah
 */
public class ConditionalAction extends AbstractAction{
   
    private String simpleCondition;
    private OptionActionInstance actionBasedCondition;
    private ArrayList<AbstractAction> consequenceActions;
    private OptionActionInstance alternativeOptions;
    
    public ConditionalAction(JSONObject conditionalJSON, long level) {
        super(level);
        Object o = conditionalJSON.get("condition");
        if (o instanceof String) {
            simpleCondition = (String) o;
        }
        else{
            actionBasedCondition = new OptionActionInstance((JSONObject)o, level);
        }
        
        Object consequences = conditionalJSON.get("consequent");
        consequenceActions = Utils.returnListOfAbstractActions(consequences, level);
        if (conditionalJSON.containsKey("alternative") && conditionalJSON.get("alternative") != null) {
            alternativeOptions = new OptionActionInstance((JSONObject) conditionalJSON.get("alternative"), level);
        }
    }
}
