/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apiinvoke;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.util.List;

/**
 *
 * @author hsaqallah
 */
public class TrainingData {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final int WORDS = 30;
        BufferedWriter trainingDataWriter = null;
        BufferedWriter testDataWriter = null;
        try {
        FileInputStream fin = new FileInputStream("action-command-nlp.out");
        ObjectInputStream ois = new ObjectInputStream(fin);
        //FileOutputStream fout = new FileOutputStream("featurematrix.out");
        trainingDataWriter = new BufferedWriter(new FileWriter("trainingfeaturematrix-2.out"));
        testDataWriter = new BufferedWriter(new FileWriter("testfeaturematrix-2.out"));
        while(true) {
            TrainExample ex = (TrainExample) ois.readObject();
            String sentence = ex.getNLCommand().getCommand();
            List<String> lemmas  = ex.getNLCommand().getLemmas();
            List<String> pos = ex.getNLCommand().getPos();
            List<String> ner = ex.getNLCommand().getNerTags();
            List<String> words = ex.getNLCommand().getWords();
            
            StringBuilder lemmasSb = new StringBuilder();
            StringBuilder posSb = new StringBuilder();
            StringBuilder nerSb = new StringBuilder();
            //sb.append(sentence);
            for (int i = 0; i < WORDS; i++) {
                lemmasSb.append("\t");
                if (i >= (lemmas.size())) {
                    lemmasSb.append("O");
                }
                else {
                    lemmasSb.append(lemmas.get(i));
                }
               
                posSb.append("\t");
                if (i >= (pos.size())) {
                    posSb.append("O");
                }
                else {
                    posSb.append(pos.get(i));
                }
                nerSb.append("\t");
                if (i >= ner.size()) {
                    nerSb.append("O");
                }
                else {
                    if (Utils.isEmailAddress(words.get(i))) {
                        nerSb.append("EMAIL");
                    }
                    else if (Utils.isCreditCard(i > 0 ? words.get(i-1) + " " + words.get(i) : words.get(i))) {
                        nerSb.append("CREDIT");
                    }
                    else if (Utils.isBankAccount(i > 0 ? words.get(i-1) + " " + words.get(i) : words.get(i))) {
                        nerSb.append("BANK_ACCOUNT");
                    }
                    else if (Utils.isHashtag(words.get(i))) {
                        nerSb.append("HASHTAG");
                    }
                    else if (Utils.isCodeRepo(words.get(i))) {
                        nerSb.append("CODE_REPO");
                    }
                    else if (Utils.isDevice(i > 0 ? words.get(i-1) + " " + words.get(i) : words.get(i)) || Utils.isDevice(words.get(i))) {
                        nerSb.append("DEVICE");
                    }
                    else {
                        nerSb.append(ner.get(i));
                    }
                }
                
            }
           String data = sentence + lemmasSb.toString() + posSb.toString() + nerSb.toString() +  "\n";
           System.out.println(data);
           if (Math.random() > -1) {
            trainingDataWriter.write(data);
           }
           else {
               testDataWriter.write(data);
           }
            trainingDataWriter.flush();
            testDataWriter.flush();
            //System.out.println(((TrainExample) ois.readObject()).getNLCommand().getLemmas());
        }

            //ois.close();
        }
       catch (Exception e) { e.printStackTrace(); 
            
        }
        
        // TODO code application logic here
    }
    
}
