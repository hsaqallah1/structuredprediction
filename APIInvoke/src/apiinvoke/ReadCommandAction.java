/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apiinvoke;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author hsaqallah
 */
public class ReadCommandAction {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JSONParser parser = new JSONParser();
        FileOutputStream fout;
        ObjectOutputStream oos;

        try {     

            Object obj = parser.parse(new FileReader(".\\mappingv3.json"));
            
            fout = new FileOutputStream("action-command-nlp.out");
            oos = new ObjectOutputStream(fout);


            JSONArray array =  (JSONArray) obj;
            for (Object e:array) {
                JSONObject jsonO = (JSONObject) e;
                JSONArray JSONActionInstances = (JSONArray)jsonO.get("action_instances");
                ArrayList<AbstractAction> actionInstances = new ArrayList<>();
                for (int i = 0; i < JSONActionInstances.size(); i++) {
                    JSONObject o = (JSONObject) JSONActionInstances.get(i);
                    if (o.containsKey("options")) {
                        actionInstances.add(new OptionActionInstance(o, i)); 
                    }
                    else if (o.containsKey("condition")){
                        actionInstances.add(new ConditionalAction(o, i));
                    }
                    else if (o.containsKey("block")) {
                        actionInstances.add(new ForEachAction(o, i));
                    }
                    else {
                        actionInstances.add(new BasicAction(o,i));
                    }
                }
                
                oos.writeObject(new TrainExample(actionInstances, Utils.NLPizeSentence((String)jsonO.get("nl_command_statment")), (Long)jsonO.get("id")));
                
                System.out.println();

            }
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }        
        // TODO code application logic here
    }
    
}
