/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apiinvoke;

/**
 *
 * @author hsaqallah
 */
public abstract class AbstractAction implements ActionInstance{
    protected long level;

    protected AbstractAction(long level) {
        this.level = level + 1;//levels start from 1, not 0. e.g. first action is level 1 
    }
    public long getLevel() {
        return level;
    }

    public void setLevel(long level) {
        this.level = level;
    }
    
}
