/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apiinvoke;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author husamsaq
 */
public class API implements Serializable {
    private ArrayList<JSONObject> data = null;
    private ArrayList<ActionParams> params = null;
    private Long id;
    private String name;
    private String desc;
    private String provider;

    public ArrayList<ActionParams> getParams() {
        return params;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public String getProvider() {
        return provider;
    }
    
    public API(JSONObject o) {
        id = (Long)o.get("id");
        name = (String)o.get("name");
        desc = (String)o.get("desc");
        provider = (String)o.get("provider");
        if (o.containsKey("data")) {
            JSONArray dataA = (JSONArray)o.get("data");
            if (dataA != null) {
                data = new ArrayList<>(Arrays.asList(Arrays.copyOf(dataA.toArray(), dataA.size(), JSONObject[].class)));
            }
            
        }
        if (o.containsKey("params")) {
            JSONArray paramsA = (JSONArray)o.get("params");
            if (paramsA != null) {

                params = new ArrayList<>();
                for (int i = 0; i < paramsA.size(); i++) {
                    params.add(new ActionParams((JSONObject)paramsA.get(i)));
                }
            }
        }
        
    }
    public ArrayList<JSONObject> getData() {
        return data;
    }
    
    
}
