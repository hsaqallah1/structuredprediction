/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apiinvoke;

import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author hsaqallah
 */
public class ForEachAction extends AbstractAction{
    
    private String simpleList;
    private OptionActionInstance advancedList;
    private ArrayList<AbstractAction> forBlockActions;

    
    public ForEachAction(JSONObject jsonO, long level) {
        super(level);
        Object o = jsonO.get("list");
        if (o instanceof String) {
            simpleList = (String) o;
        }
        else{
            advancedList = new OptionActionInstance((JSONObject)o, level);
        }
        
        Object forBlock = jsonO.get("block");
        forBlockActions = Utils.returnListOfAbstractActions(forBlock, level);
       
    }

    public String getSimpleList() {
        return simpleList;
    }

    public OptionActionInstance getAdvancedList() {
        return advancedList;
    }

    public ArrayList<AbstractAction> getForBlockActions() {
        return forBlockActions;
    }
}
