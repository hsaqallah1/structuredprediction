/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apiinvoke;

import edu.stanford.nlp.simple.Sentence;
import java.util.ArrayList;
import java.util.regex.Pattern;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author hsaqallah
 */
public class Utils {

    private static Pattern emailPattern = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
    private static Pattern ccPattern = Pattern.compile("(?i)cred(it)?(\\scards?)?");
    private static Pattern bankAccountPattern = Pattern.compile("(?i)bank(\\saccoun.)?");
    private static Pattern devicePattern = Pattern.compile("(?i)((garage(\\\\sdoor)?)|(equipment))");

    public static SentenceNLP NLPizeSentence(String sentence) {
        Sentence sent = new Sentence(sentence);
        ArrayList<String> words = new ArrayList<>(sent.words());
        ArrayList<String> posTags = new ArrayList<>(sent.posTags());
        ArrayList<String> lemmas = new ArrayList<>(sent.lemmas());
        ArrayList<String> nerTags = new ArrayList<>(sent.nerTags());

//                ArrayList<String> testStr = new ArrayList<>(2);
//                testStr.add("1");
//                testStr.add("2");
        return new SentenceNLP(sentence,
                words,
                posTags,
                lemmas,
                nerTags,
                sent.parse()
        );

    }

    public static ArrayList<AbstractAction> returnListOfAbstractActions(Object o, long level) {
        ArrayList<AbstractAction> returnList = new ArrayList<>();

        if (o instanceof JSONArray) {
            for (int i = 0; i < ((JSONArray) o).size(); i++) {
                JSONObject elem = (JSONObject) ((JSONArray) o).get(i);
                if (elem.containsKey("options")) {
                    returnList.add(new OptionActionInstance(elem, level));
                } else { //Assume Basic action
                    returnList.add(new BasicAction((JSONObject) ((JSONArray) o).get(i), level));
                }
            }
        } else if (((JSONObject) o).containsKey("options")) {
            returnList.add(new OptionActionInstance((JSONObject) o, level));
        } else {
            returnList.add(new BasicAction((JSONObject) o, level));
        }

        return returnList;
    }

    public static boolean isEmailAddress(String text) {
        return emailPattern.matcher(text).matches();
    }

    public static boolean isCreditCard(String text) {
        return ccPattern.matcher(text).matches();
    }
    
    public static boolean isBankAccount(String text) {
        return bankAccountPattern.matcher(text).matches();
    }
    
    public static boolean isHashtag(String text) {
        return text.startsWith("#");
    }
    
    public static boolean isCodeRepo(String text) {
        return text.equalsIgnoreCase("github");
    }
    
    public static boolean isDevice(String text) {
        return devicePattern.matcher(text).matches();
    }
}
