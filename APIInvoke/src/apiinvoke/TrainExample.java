/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apiinvoke;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author hsaqallah
 */
public class TrainExample implements Serializable {
    
    private ArrayList<AbstractAction> actions;
    private SentenceNLP NLCommand;
    private long id;
    
    
    public TrainExample(ArrayList<AbstractAction> actionLl, SentenceNLP NLCommand, long id) {
        this.actions = actionLl;
        this.NLCommand = NLCommand;
        this.id = id;
    }

    public ArrayList<AbstractAction> getActionLl() {
        return actions;
    }

    public SentenceNLP getNLCommand() {
        return NLCommand;
    }

    public long getId() {
        return id;
    }
    
    
}
