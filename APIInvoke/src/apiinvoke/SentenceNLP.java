/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apiinvoke;

import edu.stanford.nlp.naturalli.Polarity;
import edu.stanford.nlp.trees.Tree;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author hsaqallah
 */
public class SentenceNLP implements Serializable{
    
    private String command;
    private List<String> words;
    private List<String> pos;
    private List<String> lemmas;
    private List<String> nerTags;
    private Tree parse;

    public SentenceNLP(String command,
                        List<String> words,
                        List<String> pos,
                        List<String> lemmas,
                        List<String> nerTags,
                        Tree parse) {
        this.command = command;
        this.words = words;
        this.pos = pos;
        this.lemmas = lemmas;
        this.nerTags = nerTags;
        this.parse = parse;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public void setWords(List<String> words) {
        this.words = words;
    }

    public void setPos(List<String> pos) {
        this.pos = pos;
    }

    public void setLemmas(List<String> lemmas) {
        this.lemmas = lemmas;
    }

    public void setNerTags(List<String> nerTags) {
        this.nerTags = nerTags;
    }

    public void setParse(Tree parse) {
        this.parse = parse;
    }

    public String getCommand() {
        return command;
    }

    public List<String> getWords() {
        return words;
    }

    public List<String> getPos() {
        return pos;
    }

    public List<String> getLemmas() {
        return lemmas;
    }

    public List<String> getNerTags() {
        return nerTags;
    }

    public Tree getParse() {
        return parse;
    }


}
