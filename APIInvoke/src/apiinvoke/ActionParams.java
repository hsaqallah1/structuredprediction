/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apiinvoke;

import java.io.Serializable;
import org.json.simple.JSONObject;

/**
 *
 * @author husamsaq
 */
class ActionParams implements Serializable {
    
    private String desc;
    private String name;
    private String sample;

    public ActionParams(JSONObject o) {
        desc = (String) o.get("desc");
        name = (String) o.get("name");
        sample = (String) o.get("sample");
    }
    
}
