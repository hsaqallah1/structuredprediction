/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apiinvoke;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 *
 * @author hsaqallah
 */
public class CreateFeatureMatrix {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BufferedWriter trainingDataWriter = null;
        BufferedWriter testDataWriter = null;
        try {
        FileInputStream fin = new FileInputStream("action-command-nlp.out");
        ObjectInputStream ois = new ObjectInputStream(fin);
        //FileOutputStream fout = new FileOutputStream("featurematrix.out");
        trainingDataWriter = new BufferedWriter(new FileWriter("trainingfeaturematrix.out"));
        testDataWriter = new BufferedWriter(new FileWriter("testfeaturematrix.out"));
        int limit = 10;
        while(true) {
            TrainExample ex = (TrainExample) ois.readObject();
            List<String> lemmas  = ex.getNLCommand().getLemmas();
            List<String> pos = ex.getNLCommand().getPos();
            List<AbstractAction> actions = ex.getActionLl();
            StringBuilder sb = new StringBuilder();
            AbstractAction firstAction = actions.get(0); //It's an oversimplication to assume that the first action in the list is the one that corresponds to the natural language command
            long firstActionId = 0;
            if (firstAction instanceof OptionActionInstance) {
                firstActionId = ((OptionActionInstance)firstAction).getOptions().get(0).getActionId();
            }
            else if (firstAction instanceof BasicAction) {
                firstActionId = ((BasicAction)firstAction).getActionId();
            }
            else {
                continue; //Shameless skipping the case of ForEach actions
            }
            sb.append(firstActionId);
            for (int i = 0; i < limit; i++) {
                sb.append("\t");
                if (i >= (lemmas.size())) {
                    sb.append(".");
                }
                else {
                    sb.append(lemmas.get(i));
                }
               
                sb.append("\t");
                if (i >= (pos.size())) {
                    sb.append(".");
                }
                else {
                    sb.append(pos.get(i));
                }
            }
            sb.append("\n"); 
           System.out.println(sb);
           if (Math.random() > 0.2) {
            trainingDataWriter.write(sb.toString());
           }
           else {
               testDataWriter.write(sb.toString());
           }
            trainingDataWriter.flush();
            testDataWriter.flush();
            //System.out.println(((TrainExample) ois.readObject()).getNLCommand().getLemmas());
        }

            //ois.close();
        }
       catch (Exception e) { e.printStackTrace(); 
            
        }
        
    }
    
}
